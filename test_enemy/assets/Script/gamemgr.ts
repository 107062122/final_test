// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {



    // LIFE-CYCLE CALLBACKS
    private player;
    private move;

    onLoad () {
    cc.director.getPhysicsManager().enabled = true;
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    this.node.on(cc.Node.EventType.MOUSE_MOVE, this.mouseMove, this);
    }

    start () {
        this.player = this.node.getChildByName("zombie_head");
        this.move = this.node.getChildByName("zombie_head").getComponent('move');
    }
    onKeyDown(event){
        switch(event.keyCode)
        {   
            case cc.macro.KEY.w:
                this.move.movedir('w');
                break;
            case cc.macro.KEY.s:
                this.move.movedir('s');
                break;
            case cc.macro.KEY.a:
                this.move.movedir('a');
                break;
            case cc.macro.KEY.d:
                this.move.movedir('d');
                break;
        }
    }
    onKeyUp(event){
        switch(event.keyCode)
        {
            case cc.macro.KEY.w:
                this.move.movedir_b('w');
                break;
            case cc.macro.KEY.s:
                this.move.movedir_b('s');
                break;
            case cc.macro.KEY.a:
                this.move.movedir_b('a');
                break;
            case cc.macro.KEY.d:
                this.move.movedir_b('d');
                break;
        }
    }
    mouseMove(event){
        
        //cc.log(event.getLocation());
        let startpos = this.player.position;
        startpos.x += 480;//將cavas裡面的座標轉換成0,0在左下角的座標
        startpos.y += 320;
        let endpos = event.getLocation();

        let dirVec = endpos.sub(startpos);
        let comVec = new cc.Vec2(0, 1);
        let radian = dirVec.signAngle(comVec);
        let degree = Math.floor(cc.misc.radiansToDegrees(radian));
        
        
        
        console.log("x角度：" + (degree-90));
        this.move.rotate(degree-90);
    }
    // update (dt) {}
}
