// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    
    private hand_l;
    private hand_r; 
    private mw:boolean = false;
    private ms:boolean = false;
    private ma:boolean = false;
    private md:boolean = false;

    private movespeed:number = 100;
    // LIFE-CYCLE CALLBACKS:

    start () {
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");
        
        this.getComponent(cc.RigidBody).syncRotation(true);
    }

    rotate(degree:number){
        this.node.rotation = degree;
        
        this.hand_l.getComponent(cc.RigidBody).syncRotation(true);
        this.hand_r.getComponent(cc.RigidBody).syncRotation(true);
    }

    movedir(dir){
        if(dir == 'w'){
            this.mw = true;
        }
        else if(dir == 's'){
            this.ms = true;
        }
        else if(dir == 'a'){
            this.ma = true;
        }
        else if(dir == 'd'){
            this.md = true;
        }
    }
    movedir_b(dir){
        if(dir == 'w'){
            this.mw = false;
        }
        else if(dir == 's'){
            this.ms = false;
        }
        else if(dir == 'a'){
            this.ma = false;
        }
        else if(dir == 'd'){
            this.md = false;
        }
    }

    update (dt) {
        if(this.mw) this.node.y += this.movespeed*dt;
        else if(this.ms) this.node.y -= this.movespeed*dt

        if(this.ma) this.node.x -= this.movespeed*dt;
        else if(this.md) this.node.x += this.movespeed*dt;

        this.hand_l.getComponent(cc.RigidBody).syncPosition(true);
        this.hand_r.getComponent(cc.RigidBody).syncPosition(true);
    }
}
